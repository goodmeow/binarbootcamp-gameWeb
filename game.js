/* eslint-disable no-plusplus */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-return-assign */
/* eslint-disable no-alert */
function getComputerChoice() {
  const computer = Math.random();

  if (computer < 0.382) return 'paper';
  if (computer >= 0.733 && computer < 0.544) return 'rock';
  return 'scissors';
}

let start = null;
// eslint-disable-next-line consistent-return
function getResult(player, computer) {
  if (player === computer) return start = 'draw';
  if (player === 'scissors') return (computer === 'paper') ? start = 'win' : start = 'lose';
  if (player === 'paper') return (computer === 'rock') ? start = 'win' : start = 'lose';
  if (player === 'rock') return (computer === 'scissors') ? start = 'win' : start = 'lose';
}

// get player choice
const getPlayerChoice = document.querySelectorAll('.player-choice');
const pushResult = document.querySelector('.versus h1');
const refreshButton = document.querySelector('.refresh');

function bannerResult(sendResult) {
  if (sendResult === 'draw') {
    pushResult.innerHTML = 'DRAW';
    pushResult.style.color = 'white';
    pushResult.style.fontSize = '150%';
    pushResult.style.backgroundColor = '#4c9653';
    pushResult.style.width = '100px';
    pushResult.style.height = '50px';
    pushResult.style.padding = '10%';
  } else if (sendResult === 'win') {
    pushResult.innerHTML = 'PLAYER 1 WIN';
    pushResult.style.color = 'white';
    pushResult.style.fontSize = '150%';
    pushResult.style.backgroundColor = '#4c9653';
    pushResult.style.padding = '10%';
  } else if (sendResult === 'lose') {
    pushResult.innerHTML = 'COM WIN';
    pushResult.style.color = 'white';
    pushResult.style.fontSize = '150%';
    pushResult.style.backgroundColor = '#4c9653';
    pushResult.style.padding = '10%';
  } else {
    pushResult;
  }
}

const compBox = document.querySelectorAll('.greyBox.compImage');
const resultClass = document.querySelector('.versus div div');
function wait() {
  // eslint-disable-next-line no-shadow
  const start = new Date().getTime();
  let i = 0;

  setInterval(() => {
    if (new Date().getTime() - start >= 1000) {
      clearInterval;
      return;
    }

    compBox[i++].style.backgroundColor = '#c4c4c4';
    if (i === compBox.length) i = 0;
    resultClass.classList.remove('start');
    pushResult;
  }, 50);

  setTimeout(() => {
    setInterval(() => {
      if (new Date().getTime() - start >= 1200) {
        clearInterval;
        return;
      }
      compBox[i++].style.backgroundColor = '#9c835f';
      if (i === compBox.length) i = 0;
    }, 50);
  }, 50);
}

refreshButton.addEventListener('click', () => {
  start = null;
  pushResult.innerHTML = 'VS';
  pushResult.style.backgroundColor = '#9b835f';
  pushResult.style.fontSize = '40pt';
  pushResult.style.color = '#bd0100';
  pushResult.style.fontWeight = 'bolder';
});

getPlayerChoice.forEach((get) => {
  get.addEventListener('click', () => {
    if (start === null) {
      const computerChoice = getComputerChoice();
      const playerChoice = get.classList[0];
      getResult(playerChoice, computerChoice);
      wait();
      bannerResult(start);
    } else if (start) {
      alert('Refresh the page or click refresh button.');
    }
  });
});
